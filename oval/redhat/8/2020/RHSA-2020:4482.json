{
  "XMLName": {
    "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
    "Local": "definition"
  },
  "ID": "oval:com.redhat.rhsa:def:20204482",
  "Class": "patch",
  "Title": "RHSA-2020:4482: libgcrypt security, bug fix, and enhancement update (Moderate)",
  "Affecteds": [
    {
      "XMLName": {
        "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
        "Local": "affected"
      },
      "Family": "unix",
      "Platforms": [
        "Red Hat Enterprise Linux 8"
      ]
    }
  ],
  "References": [
    {
      "XMLName": {
        "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
        "Local": "reference"
      },
      "Source": "RHSA",
      "RefID": "RHSA-2020:4482",
      "RefURL": "https://access.redhat.com/errata/RHSA-2020:4482"
    },
    {
      "XMLName": {
        "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
        "Local": "reference"
      },
      "Source": "CVE",
      "RefID": "CVE-2019-13627",
      "RefURL": "https://access.redhat.com/security/cve/CVE-2019-13627"
    }
  ],
  "Description": "The libgcrypt library provides general-purpose implementations of various cryptographic algorithms.\n\nThe following packages have been upgraded to a later upstream version: libgcrypt (1.8.5). (BZ#1764918)\n\nSecurity Fix(es):\n\n* libgcrypt: ECDSA timing attack allowing private key leak (CVE-2019-13627)\n\nFor more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\n\nAdditional Changes:\n\nFor detailed information on changes in this release, see the Red Hat Enterprise Linux 8.3 Release Notes linked from the References section.",
  "Advisory": {
    "XMLName": {
      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
      "Local": "advisory"
    },
    "Severity": "Moderate",
    "Cves": [
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "cve"
        },
        "CveID": "CVE-2019-13627",
        "Cvss2": "",
        "Cvss3": "6.3/CVSS:3.0/AV:L/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N",
        "Cwe": "CWE-362",
        "Impact": "",
        "Href": "https://access.redhat.com/security/cve/CVE-2019-13627",
        "Public": "20191002"
      }
    ],
    "Bugzillas": [
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "bugzilla"
        },
        "ID": "1762741",
        "URL": "https://bugzilla.redhat.com/1762741",
        "Title": "FIPS mutex lock used from gcry_cipher_(en|de)crypt imposes severe performance penalty"
      },
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "bugzilla"
        },
        "ID": "1762765",
        "URL": "https://bugzilla.redhat.com/1762765",
        "Title": "RFE: hardware accelerated AES-XTS mode"
      },
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "bugzilla"
        },
        "ID": "1764018",
        "URL": "https://bugzilla.redhat.com/1764018",
        "Title": "CVE-2019-13627 libgcrypt: ECDSA timing attack allowing private key leak"
      }
    ],
    "AffectedCPEList": [
      "cpe:/o:redhat:enterprise_linux:8",
      "cpe:/o:redhat:enterprise_linux:8::baseos"
    ],
    "Issued": {
      "Date": "2020-11-04"
    },
    "Updated": {
      "Date": "2020-11-04"
    }
  },
  "Criteria": {
    "XMLName": {
      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
      "Local": "criteria"
    },
    "Operator": "OR",
    "Criterias": [
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "criteria"
        },
        "Operator": "AND",
        "Criterias": [
          {
            "XMLName": {
              "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
              "Local": "criteria"
            },
            "Operator": "OR",
            "Criterias": [
              {
                "XMLName": {
                  "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                  "Local": "criteria"
                },
                "Operator": "AND",
                "Criterias": null,
                "Criterions": [
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482001",
                    "Comment": "libgcrypt is earlier than 0:1.8.5-4.el8"
                  },
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482002",
                    "Comment": "libgcrypt is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "XMLName": {
                  "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                  "Local": "criteria"
                },
                "Operator": "AND",
                "Criterias": null,
                "Criterions": [
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482003",
                    "Comment": "libgcrypt-debugsource is earlier than 0:1.8.5-4.el8"
                  },
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482004",
                    "Comment": "libgcrypt-debugsource is signed with Red Hat redhatrelease2 key"
                  }
                ]
              },
              {
                "XMLName": {
                  "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                  "Local": "criteria"
                },
                "Operator": "AND",
                "Criterias": null,
                "Criterions": [
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482005",
                    "Comment": "libgcrypt-devel is earlier than 0:1.8.5-4.el8"
                  },
                  {
                    "XMLName": {
                      "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
                      "Local": "criterion"
                    },
                    "Negate": false,
                    "TestRef": "oval:com.redhat.rhsa:tst:20204482006",
                    "Comment": "libgcrypt-devel is signed with Red Hat redhatrelease2 key"
                  }
                ]
              }
            ],
            "Criterions": null
          }
        ],
        "Criterions": [
          {
            "XMLName": {
              "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
              "Local": "criterion"
            },
            "Negate": false,
            "TestRef": "oval:com.redhat.rhba:tst:20193384074",
            "Comment": "Red Hat Enterprise Linux 8 is installed"
          }
        ]
      }
    ],
    "Criterions": [
      {
        "XMLName": {
          "Space": "http://oval.mitre.org/XMLSchema/oval-definitions-5",
          "Local": "criterion"
        },
        "Negate": false,
        "TestRef": "oval:com.redhat.rhba:tst:20193384075",
        "Comment": "Red Hat Enterprise Linux must be installed"
      }
    ]
  }
}